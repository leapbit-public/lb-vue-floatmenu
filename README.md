<br><br>

<h3>lb-vue-floatmenu</h3>

<br>
<p><img src="https://www.leapbit.com/branding/logo.png" style="width: 300px;"></p>

<br>

## Screenshot

<p><img src="https://gitlab.com/leapbit-public/lb-vue-floatmenu/raw/master/screen.png" alt="lb-vue-floatmenu" ></p>

## Description

<p align="center">
  Vue2.js floating for usage with right click (context menu) or any other required floating parts
</p>

<br><br><br><br><br>

## Install

``` bash
npm install lb-vue-floatmenu --save
```

Include plugin in your `main.js` file.

```js
import Vue from 'vue'
import lbVueFloatmenu from 'lb-vue-floatmenu';
Vue.component('lb-vue-floatmenu', lbVueFloatmenu);
```

## Using

template code

```html
<template>
    <div v-on:contextmenu.prevent="$refs.someref.open($event, {name: 'test'})"></div>
    <lb-vue-floatmenu ref="someref">
        <template v-slot="{item}">
            <ul v-if="item">
                <li>Some option {{item.name}}</li>
            </ul>
        </template>
    </lb-vue-floatmenu>
</template>
```

template code when in v-for

```html
<template>
    <div v-for="item in items">
        <div v-on:contextmenu.prevent="$refs['someref'+item.id][0].open($event, item)"></div>
        <lb-vue-floatmenu :ref="someref[item.id]">
            <template v-slot="{item}">
                <ul v-if="item">
                    <li>Some option {{item.name}}</li>
                </ul>
            </template>
        </lb-vue-floatmenu>
    </div>
</template>
```

<br><br>


## Available settings

| Property | Type | Required | Description |
| :---------------- | :-- | :-- | :-- |
| ref | string | * | reference name for context menu |

<br><br>