// Import vue component
import component from './src/FloatMenu.vue';

// To allow use as module (npm/webpack/etc.) export component
export default component;