// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Dev from './Dev'
import FloatMenu from '@/FloatMenu'
//import FloatMenu from '../dist/lb-vue-floatmenu'

Vue.component('floatmenu', FloatMenu);

new Vue({
	render: h => h(Dev)
}).$mount('#app')
